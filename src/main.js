import Vue from 'vue'
import App from './App.vue'
import router from './router'
import "@/assets/styles.css"

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fab)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
 
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'
Vue.use(VModal)
 
//Vue toast Notification
import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';
Vue.use(VueToast);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
