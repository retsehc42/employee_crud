 
import firebase from 'firebase/app';
import 'firebase/firestore'

// const settings = { timestampsInSnapshots: true }

const config = {
    apiKey: "AIzaSyDhtzROIccyiPiTjV6C1jYXBzVWhV5UOOg",
    authDomain: "employee-management-58cb3.firebaseapp.com",
    projectId: "employee-management-58cb3",
    storageBucket: "employee-management-58cb3.appspot.com",
    messagingSenderId: "646278514231",
    appId: "1:646278514231:web:88cc07d678be88286461b2",
    measurementId: "G-8WZ0TX15TG"
  };

firebase.initializeApp(config);

const db = firebase.firestore();

db.settings({ timestampsInSnapshots: true})

export default db;